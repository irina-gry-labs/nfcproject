package com.example.user.nfchospapp.services;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.activities.PatientInfoActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by user on 11.04.2017.
 */

public class UpdatePatientInfoTask extends AsyncTask<String, String, String>

    {

        private Context context;
        PatientInfoActivity parentActivity;
        private int byGetOrPost = 0;

        String diagnosis, precription, precautions;

        //flag 0 means get and 1 means post.(By default it is get.)
        public UpdatePatientInfoTask(
                Context context,
            String diagnosis,
            String precription,
            String precautions
    ) {
            this.context = context;
        this.diagnosis = diagnosis;
        this.precription = precription;
        this.precautions = precautions;


    }

    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... arg0) {

        try {
            String patientID = (String) arg0[0];


            String link = context.getResources().getString(R.string.serverUrl) + "updatePatient.php";
            String data = URLEncoder.encode("patientID", "UTF-8") + "=" +
                    URLEncoder.encode(patientID, "UTF-8");
            data += "&" + URLEncoder.encode("diagnosis", "UTF-8") + "=" +
                    URLEncoder.encode(diagnosis, "UTF-8");
            data += "&" + URLEncoder.encode("precriptions", "UTF-8") + "=" +
                    URLEncoder.encode(precription, "UTF-8");
            data += "&" + URLEncoder.encode("precautions", "UTF-8") + "=" +
                    URLEncoder.encode(precautions, "UTF-8");

            System.out.println(patientID+" "+diagnosis+" "+precription+" "+precautions);

            URL url = new URL(link);
            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;

            // Read Server Response
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }

            return sb.toString();
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }

    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase("EMPTY DATA!")) {
            Toast.makeText(context, "Error of Updating", Toast.LENGTH_LONG).show();
        } else {
            if (result != null) {
                Toast.makeText(context, result, Toast.LENGTH_LONG).show();
            }
        }
    }
}

