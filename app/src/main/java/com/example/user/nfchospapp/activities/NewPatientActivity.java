package com.example.user.nfchospapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.models.Patient;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import static android.os.Environment.DIRECTORY_PICTURES;
import static android.os.Environment.getExternalStoragePublicDirectory;

/**
 * Created by user on 11.01.2017.
 */

public class NewPatientActivity extends AppCompatActivity {

    Button buttonPost;
    EditText firstName, secondName, age, diagnosis, prescription, precautions;
    RadioGroup sex;
    String sexP;
    TextView messageText;
    Button uploadButton;
    int serverResponseCode = 0;
    ProgressDialog dialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpatient);

        buttonPost = (Button) findViewById(R.id.button2);
        sex = (RadioGroup) findViewById(R.id.sex);
        firstName = (EditText) findViewById(R.id.firstName);
        secondName = (EditText) findViewById(R.id.secondName);
        age = (EditText) findViewById(R.id.age);
        diagnosis = (EditText) findViewById(R.id.diagnosis);
        prescription = (EditText) findViewById(R.id.prescription);
        precautions = (EditText) findViewById(R.id.precautions);



        sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:

                        break;
                    case R.id.radioButton4:
                        sexP="male";
                        break;
                    case R.id.radioButton3:
                        sexP="female";
                        break;
                    default:
                        break;
                }
            }
        });


    }

    public void savePatient(View v) {

        String firstNameP = firstName.getText().toString();
        String secondNameP = secondName.getText().toString();

        String ageP=age.getText().toString();;
        String diagnosisP = diagnosis.getText().toString();;
        String prescriptionP = prescription.getText().toString();
        String precautionsP = precautions.getText().toString();


        Patient patient = new Patient(firstNameP, sexP, secondNameP, ageP, diagnosisP, prescriptionP, precautionsP);

        AsyncTask tache = new RegisterTask().execute(patient);



    }

    class RegisterTask extends AsyncTask<Patient, Void, String> {



        @Override
        protected String doInBackground(Patient... people) {
            Patient patient = people[0];
            try {
                String firstNameP = patient.getFirstName();
                String secondNameP = patient.getSecondName();
                String sexP = patient.getSex();
                String ageP= patient.getAge();
                String diagnosisP = patient.getDiagnosis();
                String prescriptionP = patient.getPrescription();
                String precautionsP = patient.getPrecautions();

                String link = getResources().getString(R.string.serverUrl)+"addPatient.php";
                String data = URLEncoder.encode("firstName", "UTF-8") + "=" +
                        URLEncoder.encode(firstNameP, "UTF-8");
                data += "&" + URLEncoder.encode("lastName", "UTF-8") + "=" +
                        URLEncoder.encode(secondNameP, "UTF-8");
                data += "&" + URLEncoder.encode("gender", "UTF-8") + "=" +
                        URLEncoder.encode(sexP, "UTF-8");
                data += "&" + URLEncoder.encode("age", "UTF-8") + "=" +
                        URLEncoder.encode(ageP, "UTF-8");
                data += "&" + URLEncoder.encode("diagnosis", "UTF-8") + "=" +
                        URLEncoder.encode(diagnosisP, "UTF-8");
                data += "&" + URLEncoder.encode("precriptions", "UTF-8") + "=" +
                        URLEncoder.encode(prescriptionP, "UTF-8");
                data += "&" + URLEncoder.encode("precautions", "UTF-8") + "=" +
                        URLEncoder.encode(precautionsP, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         //   showProgressDialog(true);

        }

        @Override
        protected void onPostExecute(String result) {
            if(result==null){
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.errorAddingPatient), Toast.LENGTH_LONG).show();
            }
            else{
                Intent intent = new Intent(NewPatientActivity.this, WriteTagActivity.class);
                intent.putExtra("new_patient_id", (Serializable) result);
                startActivity(intent);

            }
            //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
        }


    }
}
