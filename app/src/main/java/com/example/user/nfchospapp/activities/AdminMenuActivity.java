package com.example.user.nfchospapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.services.SigninTask;

/**
 * Created by user on 11.04.2017.
 */

public class AdminMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminmenu);
    }

    public void newPatientAction(View view){
        Intent intent = new Intent(AdminMenuActivity.this, NewPatientActivity.class);
        startActivity(intent);

    }

    public void newDoctorAction(View view){
        Intent intent = new Intent(AdminMenuActivity.this, NewDoctorActivity.class);
        startActivity(intent);

    }
}

