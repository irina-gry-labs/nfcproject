package com.example.user.nfchospapp.services;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.activities.AdminMenuActivity;
import com.example.user.nfchospapp.activities.NewPatientActivity;
import com.example.user.nfchospapp.activities.PatientInfoActivity;
import com.example.user.nfchospapp.activities.ReadTagActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by user on 11.01.2017.
 */

public class SigninTask extends AsyncTask<String, String, String> {
    private TextView statusField, roleField;
    private Context context;
    private int byGetOrPost = 0;

    //flag 0 means get and 1 means post.(By default it is get.)
    public SigninTask(Context context, TextView statusField, TextView roleField) {
        this.context = context;
        this.statusField = statusField;
        this.roleField = roleField;

    }

    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... arg0) {

        try {
            String username = (String) arg0[0];
            String password = (String) arg0[1];

            String link = context.getResources().getString(R.string.serverUrl)+"auth.php";
            String data = URLEncoder.encode("username", "UTF-8") + "=" +
                    URLEncoder.encode(username, "UTF-8");
            data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                    URLEncoder.encode(password, "UTF-8");

            URL url = new URL(link);
            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;

            // Read Server Response
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }

            return sb.toString();
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }

    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase("Error!")) {
            Toast.makeText(context, "Username or password is incorrect", Toast.LENGTH_LONG).show();
        } else {
            if (result != null) {
            //     Toast.makeText(context, result, Toast.LENGTH_LONG).show();


                try {
                    JSONObject jsonObj = new JSONObject(result);

                    // Getting JSON Array node
                  //  JSONArray users = jsonObj.getJSONArray("docData");

                    // looping through All Contacts
                 //   for (int i = 0; i < users.length(); i++) {
                     //   JSONObject c = users.getJSONObject(i);

                        String id = jsonObj.getString("id");
                        String fName = jsonObj.getString("fName");
                        String lName = jsonObj.getString("lName");
                        String spec = jsonObj.getString("spec");
                        String role = jsonObj.getString("role");
                    if (role.equalsIgnoreCase("doctor")) {
                        Intent intent = new Intent(context, ReadTagActivity.class);

                        context.startActivity(intent);

                    }
                    if (role.equalsIgnoreCase("admin")) {
                        Intent intent = new Intent(context, AdminMenuActivity.class);
                        context.startActivity(intent);
                    }
                       // Toast.makeText(context, role, Toast.LENGTH_LONG).show();
               //    }
                } catch (final JSONException e) {
                  e.printStackTrace();
                    }



            }
        }
        // this.statusField.setText("Login Successful");
        //   this.roleField.setText(result);
    }
}