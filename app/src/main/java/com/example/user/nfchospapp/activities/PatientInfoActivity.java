package com.example.user.nfchospapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.services.GetPatientInfoTask;
import com.example.user.nfchospapp.services.SigninTask;
import com.example.user.nfchospapp.services.UpdatePatientInfoTask;

/**
 * Created by user on 12.01.2017.
 */

public class PatientInfoActivity extends AppCompatActivity {
    EditText diagnosis;
    TextView  precription, precautions;
    String idP;
    Button editBtn;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infopatient);
        context = PatientInfoActivity.this;

        diagnosis = (EditText) findViewById(R.id.diagnosis);
        precription = (TextView) findViewById(R.id.precription);
        precautions = (TextView) findViewById(R.id.precautions);
        Intent i = getIntent();
        idP = (String) i.getSerializableExtra("curr_patient_id");

        if (idP != null) {

            new GetPatientInfoTask(PatientInfoActivity.this, this, diagnosis, precription, precautions).execute(idP);
        } else
            Toast.makeText(this, "Patient's id is null", Toast.LENGTH_LONG).show();

        editBtn = (Button) findViewById(R.id.btn_editInfo);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 new UpdatePatientInfoTask(context,diagnosis.getText().toString(), precription.getText().toString(),
                         precautions.getText().toString()).execute(idP);
            }
        });
    }
    }






