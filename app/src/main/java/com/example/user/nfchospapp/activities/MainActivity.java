package com.example.user.nfchospapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.services.SigninTask;

public class MainActivity extends AppCompatActivity {

    private EditText usernameField,passwordField;
    private TextView status,role,method;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      

        usernameField = (EditText)findViewById(R.id.editText1);
        passwordField = (EditText)findViewById(R.id.editText2);
        usernameField.setPadding(30,0,0,0);
        passwordField.setPadding(30,0,0,0);
   //     status = (TextView)findViewById(R.id.textView6);
     //   role = (TextView)findViewById(R.id.textView7);
     //  method = (TextView)findViewById(R.id.textView9);
    }

    public void loginPost(View view){
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
       // method.setText("Post Method");
        new SigninTask(this,status,role).execute(username,password);
    }
}
