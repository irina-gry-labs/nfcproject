package com.example.user.nfchospapp.services;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.activities.NewPatientActivity;
import com.example.user.nfchospapp.activities.PatientInfoActivity;
import com.example.user.nfchospapp.activities.ReadTagActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by user on 12.01.2017.
 */

public class GetPatientInfoTask extends AsyncTask<String, String, String> {

    private Context context;
    PatientInfoActivity parentActivity;
    private int byGetOrPost = 0;

    EditText diagnosis;
    TextView  precription, precautions;

    //flag 0 means get and 1 means post.(By default it is get.)
    public GetPatientInfoTask(Context context,
                              PatientInfoActivity parentActivity,
                           //   TextView fName,
                           //   TextView lName,
                              EditText diagnosis,
                              TextView precription,
                              TextView precautions
                              ) {
        this.context = context;
       // this.fName = fName;
       // this.lName = lName;
        this.parentActivity = parentActivity;
        this.diagnosis = diagnosis;
        this.precription = precription;
        this.precautions = precautions;


    }

    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... arg0) {

        try {
            String patientID = (String) arg0[0];


            String link = context.getResources().getString(R.string.serverUrl) + "patient.php";
            String data = URLEncoder.encode("patientID", "UTF-8") + "=" +
                    URLEncoder.encode(patientID, "UTF-8");

            URL url = new URL(link);
            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;

            // Read Server Response
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }

            return sb.toString();
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }

    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase("EMPTY DATA!")) {
            Toast.makeText(context, "Patient's ID is incorrect", Toast.LENGTH_LONG).show();
        } else {
            if (result != null) {
                //     Toast.makeText(context, result, Toast.LENGTH_LONG).show();


                try {
                    JSONObject jsonObj = new JSONObject(result);

                    // Getting JSON Array node
                    //  JSONArray users = jsonObj.getJSONArray("docData");

                    // looping through All Contacts
                    //   for (int i = 0; i < users.length(); i++) {
                    //   JSONObject c = users.getJSONObject(i);


                    String fNameP = jsonObj.getString("fName");
                    String lNameP = jsonObj.getString("lName");
                    String diagnosisP = jsonObj.getString("diagnosis");
                    String precriptionP = jsonObj.getString("precriptions");
                    String precautionsP = jsonObj.getString("precautions");

                   // fName.setText(fNameP);
                    //lName.setText(lNameP);
                    diagnosis.setText(diagnosisP);
                    precription.setText(precriptionP);
                    precautions.setText(precautionsP);
                    parentActivity.setTitle("Patient: "+fNameP+" "+lNameP);
                    // Toast.makeText(context, role, Toast.LENGTH_LONG).show();
                    //    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                }


            }
        }
        // this.statusField.setText("Login Successful");
        //   this.roleField.setText(result);
    }
}
