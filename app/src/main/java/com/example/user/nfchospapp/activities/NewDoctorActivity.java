package com.example.user.nfchospapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.nfchospapp.R;
import com.example.user.nfchospapp.models.Doctor;
import com.example.user.nfchospapp.models.Patient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by user on 11.04.2017.
 */

public class NewDoctorActivity extends AppCompatActivity {

    Button buttonPost;
    EditText login, pass, firstName, secondName, specialization;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newdoctor);

        buttonPost = (Button) findViewById(R.id.saveDoctorBtn);
        login = (EditText)findViewById(R.id.login);
        pass = (EditText)findViewById(R.id.pass);
        firstName = (EditText)findViewById(R.id.firstName);
        secondName = (EditText)findViewById(R.id.secondName);
        specialization = (EditText)findViewById(R.id.spec);


    }

    public void saveDoctor(View v) {

        String firstNameP = firstName.getText().toString();
        String secondNameP = secondName.getText().toString();

        String loginP=login.getText().toString();;
        String passP = pass.getText().toString();;
        String specializationP = specialization.getText().toString();


        Doctor doctor = new Doctor(firstNameP, secondNameP, loginP, passP, specializationP);

        AsyncTask tache = new NewDoctorActivity.RegisterDocTask().execute(doctor);



    }

    class RegisterDocTask extends AsyncTask<Doctor, Void, String> {



        @Override
        protected String doInBackground(Doctor... people) {
            Doctor doctor = people[0];
            try {
                String firstNameP = doctor.getFirstName();
                String secondNameP = doctor.getSecondName();
                String loginP = doctor.getLogin();
                String passP= doctor.getPassword();
                String specP = doctor.getSpecialization();

                String link = getResources().getString(R.string.serverUrl)+"addDoctor.php";
                String data = URLEncoder.encode("login", "UTF-8") + "=" +
                        URLEncoder.encode(loginP, "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" +
                        URLEncoder.encode(passP, "UTF-8");
                data += "&" + URLEncoder.encode("firstName", "UTF-8") + "=" +
                        URLEncoder.encode(firstNameP, "UTF-8");
                data += "&" + URLEncoder.encode("lastName", "UTF-8") + "=" +
                        URLEncoder.encode(secondNameP, "UTF-8");
                data += "&" + URLEncoder.encode("spec", "UTF-8") + "=" +
                        URLEncoder.encode(specP, "UTF-8");
                data += "&" + URLEncoder.encode("role", "UTF-8") + "=" +
                        URLEncoder.encode("doctor", "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write(data);
                wr.flush();

                BufferedReader reader = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                return sb.toString();
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   showProgressDialog(true);

        }

        @Override
        protected void onPostExecute(String result) {
            if(result==null){
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.errorAddingPatient), Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            }

        }



}

}
